<?php
/**
 * Plugin Name:       Grupo XXI Slider
 * Plugin URI:        http://grupoxxi.com
 * Description:       Slider con widget para Transporte XXI
 * Version:           0.2.0
 * Author:            Paulo Carvajal
 * Author URI:        http://paulocarvajal.com
 * Text Domain:       slider_xxi
 * Domain Path:       /lang
 */


/**
 * Changelog
 *
 * 0.2.0
 * Added new category widget
 * Swiper updated to 3.1.7
 *
 */

 // Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) exit;


if( ! class_exists( 'GXXI_Slider' ) ) {
/**
 * GXXI_Slider class
 *
 * @class   GXXI_Slider
 * @version 0.1.0
 */

class GXXI_Slider{
  /**
   * @var GXXI_Slider The single instance of the class
   * @since 0.1.0
   */
  protected static $_instance = null;

  /**
   * @var string
   */
  public $version = '0.1.0';

  /**
   * @var string
   */
  public $slug = 'slider_xxi';


  /**
   * Main GXXI_Slider instance
   *
   * Ensures only one instance of GXXI_Slider is loaded or can be loaded.
   *
   * @since 0.1.0
   * @static
   * @return GXXI_Slider, main instance
   */
  public static function instance() {
    if( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  /**
   * Constructor.
   *
   * @since   0.1.0
   * @access  public
   * @uses    GXXI_Slider::be_display_posts_plugin()
   * @return  void
   */
  public function __construct()  {

    // Required files
    $file_includes = [
      'inc/post-types.php',
      'widgets/widget-slider.php',
      'widgets/widget-slider-category.php',
    ];

    // includes files
    foreach ($file_includes as $file) {
      include( $this->plugin_path() . $file );
    }
    unset($file);


    add_action( 'init', array( $this, 'init' ), 10 );

    add_action( 'widgets_init', array( $this, 'widgets_init'), 20);

    // front end actions
    if( ! is_admin() || defined( 'DOING_AJAX' ) ) {
      add_action( 'wp_enqueue_scripts',   array( $this, 'wp_enqueue_scripts' ),   25 );
    }

    // add plugin row meta
    if( is_admin() ) {

    }


    // action hook
    do_action( $this->slug . '_loaded' );
  }

  /**
   * Plugin init.
   * Registers init actions and creates post types
   *
   * @since   0.1.0
   * @access  public
   * @return  void
   */
  public function init() {
    // load localization files
    load_plugin_textdomain( $this->slug, false, plugin_basename( dirname( __FILE__ ) ) . '/lang/' );


    // create post types
    $this->create_post_types();

    //Registro de los widgets
    //add_action( 'widgets_init', array( $this, 'widgets_init'), 20);

    // action hook
    do_action( $this->slug . '_init' );
  }


  /**
   * Register widgets init actions
   * @since 0.1.0
   * @access public
   * @return void
   */
  public function widgets_init(){
    register_widget( 'Widget_Slider_XXI' );
    register_widget( 'Widget_Category_Slider_XXI' );
  }

  /**
   * Carousel shortcode callback.
   *
   * @since   0.1.0
   * @access  public
   * @param   array   $atts   Array of user defined shortcode attributes.
   * @return  string          Carousel HTML.
   */
  public function shortcode( $atts ){
    // TODO
  }

  /**
   * Enqueue scripts.
   *
   * @since   0.1.0
   * @access  public
   * @static
   * @param   bool    $thickbox   Whether to enqueue thickbox scripts and styles. Default: false.
   * @return  void
   */
  public static function enqueue( $thickbox = false ) {
    // enqueue carousel script
    if( ! wp_script_is( 'wp-bootstrap-carousel-init', 'enqueued' ) ) {
        wp_enqueue_script( 'wp-bootstrap-carousel-init' );
    }

    // enqueue thickbox styles & script
    if( $thickbox && ! wp_script_is( 'thickbox', 'enqueued' ) ) {
      add_thickbox();
    }

  }

  /**
   * Register scripts, enqueue styles.
   *
   * @since   0.1.0
   * @access  public
   * @uses    GXXI_Slider::plugin_url()
   * @return  void
   */
  public function wp_enqueue_scripts(){
    // script debug variable
    $min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

    // register carousel scripts
    wp_enqueue_script( 'swiper', $this->plugin_url() . '/js/swiper.jquery' . $min . '.js', array( 'jquery' ), $this->version, true );
    //wp_register_script( 'wp-bootstrap-carousel-init', $this->plugin_url() . '/js/carousel-init' . $min . '.js', array( 'jquery', 'wp-bootstrap-carousel' ), $this->version, true );

    // enqueue styles
    wp_enqueue_style( 'swiper', $this->plugin_url() . '/css/swiper' . $min . '.css', array(), $this->version, 'screen' );
  }

  /**
   * Instantiate Post Types Class
   * @since  0.1.0
   * @return void
   */
  public function create_post_types(){
    // Post type Example:
    // http://codex.wordpress.org/Function_Reference/register_post_type#Example
    // Tax example:
    // http://codex.wordpress.org/Function_Reference/register_taxonomy#Example
    // Icons:
    // http://melchoyce.github.io/dashicons/

    // Post types and taxs.definitions
    $my_posts = array(
      array(
        'name'               => 'gxxi_slide',
        'singular_label'     => 'Slide',
        'plural_label'       => 'Slides',
        'hierarchical'       => false,
        'has_archive'        => false,
        'public'             => true,
        'publicly_queryable' => true,
        'show_in_nav_menus'  => false,
        'menu_position'      => 50,
        'menu_icon'          => 'dashicons-images-alt2',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        //'rewrite'            => array( 'slug' => 'autor' ),
      )
    );

    $my_taxonomies = array(
      array(
        'name'                => 'gxxi_slider',
        'singular_label'      => 'Slider group',
        'plural_label'        => 'Slider groups',
        'show_ui'             => true,
        'show_in_nav_menus'   => false,
        'show_admin_column'   => true,
        //'query_var'           => 'edicion',
        //'rewrite'             => array( 'slug' => 'edicion' ),
        'hierarchical'        => true
      )
    );

    // instantiate Class
    $post_types = new GXXI_PostTypes( $my_posts );
    $post_types->create_taxonomies($my_taxonomies, array('gxxi_slide') );
  }


  /**
   * Get the plugin url.
   *
   * @since   0.1.0
   * @access  public
   * @return  string  The plugin url.
   */
  public function plugin_url() {
    return untrailingslashit( plugins_url( '/', __FILE__ ) );
  }
  /**
   * Get the plugin path.
   *
   * @since   0.1.0
   * @access  public
   * @return  string  The plugin path.
   */
  public function plugin_path() {
    return plugin_dir_path( __FILE__ );
  }


}// end class GXXI_Slider

/**
 * Init GXXI_Slider class.
 * Initializes the main plugin class.
 * @since 0.1.0
 */

GXXI_Slider::instance();

}// end if not class exists GXXI_Slider



//define( 'GXXI_SLIDER_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
//define( 'GXXI_SLIDER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );



