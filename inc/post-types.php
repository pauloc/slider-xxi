<?php
/**
 * Creates post types and taxonomies for gxxi_slider
 */
if( ! class_exists( 'GXXI_PostTypes' ) ) {
class GXXI_PostTypes {

  private static $_this;

  private $defaults;
  private $tax_defaults;
  private $post_types;

  function __construct( $post_types ) {
    // Singleton
    if ( isset( self::$_this ) ) return $this;
      //wp_die( );

    self::$_this = $this;

    $this->post_types = $post_types;
    $this->create_defaults();
    $this->create_post_types();

    // Creates featured image column
    add_filter( 'manage_edit-gxxi_slide_columns', array( $this, 'add_gxxi_slide_columns' ) );
    add_action( 'manage_gxxi_slide_posts_custom_column', array( $this, 'add_gxxi_slide_columns_content' ), 10, 2);

    //load_plugin_textdomain('gxxi_slider_post_types', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    //add_action( 'init', array( $this, 'create_post_types' ), 10 );
  }

  function this() {
    return self::$_this;
  }


/**
 * Creates each post type and calls 'create_taxonomies'
 * @since 0.1.0
 */
  function create_post_types() {

    foreach ( $this->post_types as $type ) {
      $name = $type['name'];
      $taxonomies = isset($type['taxonomies']) ? $type['taxonomies'] : null;

      $type['labels'] = array(
        'name' => $type['plural_label'],
        'singular_name' => $type['singular_label'],
        'menu_name' => $type['plural_label']
      );

      if ( count($taxonomies) ) {
        $this->create_taxonomies( $taxonomies, $name );
      }

      unset($type['name']);
      unset($type['singular_label']);
      unset($type['plural_label']);
      unset($type['taxonomies']);

      $args = wp_parse_args( $type, $this->defaults );

      register_post_type( $name, $args );

    }// foreach
  }

/**
 * Create taxonomies for each post type
 * @param  array $taxonomies Each post type taxonomies array
 * @param  string $type       The post type
 * @since 0.1.0
 */
  public function create_taxonomies( $taxonomies, $type ){

    foreach ( $taxonomies as $tax ) {
      $name = $tax['name'];

      $tax['labels'] = array(
        'name' => $tax['plural_label'],
        'singular_name' => $tax['singular_label']
      );

      unset($tax['name']);
      unset($tax['singular_label']);
      unset($tax['plural_label']);

      $args = wp_parse_args( $tax, $this->tax_defaults );

      register_taxonomy( $name, $type, $args );

    }// foreach

  }


  /**
   * Filtramos y añadimos columnas
   * manage_edit-{$post_type}_columns
   */
  public function add_gxxi_slide_columns( $columns ) {
    //var_dump($columns);exit;
    $res = array_slice($columns, 0, 2, true) +
      array("featured_img" => "Featured image") +
      array_slice($columns, 2, count($columns) - 1, true) ;
    return $res;
  }

  /**
   * Añadimos contenido a las columnas
   * manage_{$post_type}_posts_custom_column
   */
  public function add_gxxi_slide_columns_content ( $column, $post_id ) {
    //global $post;
    add_thickbox();

    switch( $column ) {
      /* If displaying the 'featured_img' column. */
      case 'featured_img' :
        /* Get the post meta. */
        $img = get_the_post_thumbnail( $post_id, array( 120, 120 ), array( 'class' => 'thumb item', 'data-img' => 'my_data' ) );
        $img_big = get_the_post_thumbnail( $post_id, 'medium' );

        $pic = '<div id="my-content-id" style="display:none;">'. $img_big .'</div>';

        if ( empty( $img ) )
          echo '* No picture defined *';
        else
          echo '<a href="h#TB_inline?width=600&height=550&inlineId=my-content-id" class="thickbox" title="View bigger" >'. $img .'</a>'. $pic;
        break;

      default :
        break;
    }
  }



/**
 * Create defaults values for posts and taxonomies
 * @since 0.1.0
 */
  private function create_defaults(){

    $post_defaults = array(
      'labels'               => array(),
      'description'          => '',
      'public'               => false,
      'hierarchical'         => false,
      'exclude_from_search'  => null,
      'publicly_queryable'   => null,
      'show_ui'              => null,
      'show_in_menu'         => null,
      'show_in_nav_menus'    => null,
      'show_in_admin_bar'    => null,
      'menu_position'        => null,
      'menu_icon'            => null,
      'capability_type'      => 'post',
      'capabilities'         => array(),
      'map_meta_cap'         => null,
      'supports'             => array(),
      'register_meta_box_cb' => null,
      'taxonomies'           => array(),
      'has_archive'          => false,
      'rewrite'              => true,
      'query_var'            => true,
      'can_export'           => true,
      'delete_with_user'     => null,
    );

    $this->defaults = $post_defaults;

    $tax_defaults = array(
      'labels'                => array(),
      'description'           => '',
      'public'                => true,
      'hierarchical'          => false,
      'show_ui'               => null,
      'show_in_menu'          => null,
      'show_in_nav_menus'     => null,
      'show_tagcloud'         => null,
      'meta_box_cb'           => null,
      'capabilities'          => array(),
      'rewrite'               => true,
      'query_var'             => '',
      'update_count_callback' => '',
      //'_builtin'              => false,
    );
    $this->tax_defaults = $tax_defaults;

  }

}// end class
}// if class exists