$(document).ready(function () {
  //initialize swiper when document ready
  // http://www.idangero.us/swiper/api/
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    loop: false,
    //direction: 'vertical',
    //scrollbar: '.swiper-scrollbar',
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    //grabCursor: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    centeredSlides: true,
    //autoplay: 2500,
    autoplay: 5000,
    autoplayDisableOnInteraction: true
  })
});