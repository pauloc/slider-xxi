<p>
  <label for="widget_protas_title"><?php _e( 'Title' ); ?>:</label>
  <input type="text" class="widefat" id="widget_protas_title" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
  <hr>
</p>
<p>
  <label for="widget_slider_cat"><?php _e( 'Slider group' ); ?>:</label>
  <?php echo $sldr_select; ?>
</p>
<p>
  <label for="widget_slider_cat_count"><?php _e( 'Slider count' ); ?>:</label>
  <?php echo $sldr_count; ?>
</p>