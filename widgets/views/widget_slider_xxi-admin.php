<p>
  <label for="widget_protas_title"><?php _e( 'Title' ); ?>:</label>
  <input type="text" class="widefat" id="widget_protas_title" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
  <hr>
</p>
<p>
  <label for="widget_slider_grp"><?php _e( 'Slider group' ); ?>:</label>
  <?php echo $sldr_select; ?>
</p>