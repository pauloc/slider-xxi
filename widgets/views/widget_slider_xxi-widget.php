<?php
// Contenido a mostrar en la parte pública.
?>
<script>
$(document).ready(function () {
  //initialize swiper when document ready
  // http://www.idangero.us/swiper/api/
  var mySwiper = new Swiper ('#swiper-container', {
    // Optional parameters
    loop: false,
    //direction: 'vertical',
    //scrollbar: '.swiper-scrollbar',
    pagination: '#swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
    //grabCursor: true,
    nextButton: '#swiper-button-next',
    prevButton: '#swiper-button-prev',
    centeredSlides: true,
    //autoplay: 2500,
    autoplay: 5000,
    autoplayDisableOnInteraction: true
  })
});
</script>
<div class="swiper-container" id="swiper-container">
  <div class="swiper-wrapper">
<?php
    if ( $slides->have_posts() ) {
      //echo '<ul>';
      global $post;
      while ( $slides->have_posts() ) {
        $slides->the_post();
?>
    <div class="swiper-slide slide-<?php echo $post->post_name; ?>">
      <h4><?php the_title(); ?></h4>
      <?php //echo '<h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>'; ?>
      <?php the_post_thumbnail(); ?>
      <?php
        // si se usa the_content() muestra el more links en los
        // templates de tipo listado (category, home, etc), pero no en single.
        // Una conducta extraña. Se evita así:
        echo get_the_content();
       ?>
    </div>
<?php
      }// while
    } else {
      echo 'No posts yet...';
    }
    wp_reset_postdata();
    wp_reset_query();

?>

  </div>
  <div class="clearfix"></div>

  <!-- If we need pagination -->
  <div class="swiper-pagination" id="swiper-pagination"></div>

  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev prevButton" id="swiper-button-prev"></div>
  <div class="swiper-button-next nextButton" id="swiper-button-next"></div>

  <!-- If we need scrollbar -->
  <div class="swiper-scrollbar"></div>
</div>
