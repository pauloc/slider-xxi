<?php
/**
 * GXXI Slider Widget
 */
class Widget_Category_Slider_XXI extends WP_Widget{

  /**
   * Unique identifier for your widget.
   *
   * The variable name is used as the text domain when internationalizing strings
   * of text. Its value should match the Text Domain file header in the main
   * widget file.
   *
   * @since    1.0.0
   * @var      string
   */
  protected $widget_slug = 'widget_category_slider_xxi';

  /*--------------------------------------------------*/
  /* Constructor
  /*--------------------------------------------------*/

  /**
   * Specifies the classname and description, instantiates the widget,
   * loads localization files, and includes necessary stylesheets and JavaScript.
   */
  public function __construct() {

    // load plugin text domain
    add_action( 'init', array( $this, 'widget_textdomain' ) );

    // Hooks fired when the Widget is activated and deactivated
    register_activation_hook( __FILE__, array( $this, 'activate' ) );
    register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

    parent::__construct(
      $this->get_widget_slug(),
      __( 'Slider XXI Category', $this->get_widget_slug() ),
      array(
        'classname'  => $this->get_widget_slug().'-class',
        'description' => __( 'Shows Slider XXI sliders by Post Category.', $this->get_widget_slug() )
      )
    );

    // Register admin styles and scripts
    //add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
    //add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

    // Register site styles and scripts
    //add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );
    //add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_scripts' ) );

    // Refreshing the widget's cached output with each new post
    add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
    add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
    add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

  } // end constructor


    /**
     * Return the widget slug.
     *
     * @since    1.0.0
     *
     * @return    Plugin slug variable.
     */
    public function get_widget_slug() {
        return $this->widget_slug;
    }

  /*--------------------------------------------------*/
  /* Widget API Functions
  /*--------------------------------------------------*/

  /**
   * Outputs the content of the widget.
   *
   * @param array args  The array of form elements
   * @param array instance The current instance of the widget
   */
  public function widget( $args, $instance ) {

    // Check if there is a cached output
    $cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

    if ( !is_array( $cache ) )
      $cache = array();

    if ( ! isset ( $args['widget_id'] ) )
      $args['widget_id'] = $this->id;

    if ( isset ( $cache[ $args['widget_id'] ] ) )
      return print $cache[ $args['widget_id'] ];

    // go on with your widget logic, put everything into a string and
    extract( $args, EXTR_SKIP );

    $widget_string = $before_widget;

    $title            = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
    $slider_cat       = wp_strip_all_tags( $instance['slider_cat'] );
    $slider_cat_count = absint( $instance['slider_cat_count'] );


    ob_start();
    if ( !empty( $title ) ) echo $before_title . $title . $after_title;

    $query_args = array(
      'post_type'      => 'post',
      'orderby'        => 'date',
      'order'          => 'DESC',
      'posts_per_page' => $slider_cat_count,
      'tax_query'      => array(
        array(
          'taxonomy' => 'category',
          //'field'  => 'slug',
          'field'    => 'term_id',
          'terms'    => [ $slider_cat ]
        ),
      ),

    );
    $slides = new WP_Query( $query_args );

    include( plugin_dir_path( __FILE__ ) . 'views/widget_category_slider_xxi-widget.php' );

    $widget_string .= ob_get_clean();
    $widget_string .= $after_widget;


    $cache[ $args['widget_id'] ] = $widget_string;

    wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );

    print $widget_string;

  } // end widget


  public function flush_widget_cache() {
      wp_cache_delete( $this->get_widget_slug(), 'widget' );
  }
  /**
   * Processes the widget's options to be saved.
   *
   * @param array new_instance The new instance of values to be generated via the update.
   * @param array old_instance The previous instance of values before the update.
   */
  public function update( $new_instance, $old_instance ) {

    $instance = $old_instance;
    $instance['title']       = wp_strip_all_tags( $new_instance['title'] );
    $instance['slider_cat']  = (int) wp_strip_all_tags( $new_instance['slider_cat'] );
    $instance['slider_cat_count']  = (int) $new_instance['slider_cat_count'];

    return $instance;

  } // end update

  /**
   * Generates the administration form for the widget.
   *
   * @param array instance The array of keys and values for the widget.
   */
  public function form( $instance ) {
    $defaults   = array( 'title' => '', 'slider_cat' => '' );
    $instance   = wp_parse_args( ( array ) $instance, $defaults );
    $title      = $instance['title'];
    $slider_cat = $instance['slider_cat'];
    $slider_cat_count = $instance['slider_cat_count'];

    $args = array(
      'orderby'            => 'name',
      'order'              => 'ASC',
      'hide_empty'         => 0,
      'echo'               => 0,
      'selected'           => $slider_cat,
      'name'               => $this->get_field_name( 'slider_cat' ),
      'id'                 => $this->get_field_id( 'slider_cat' ),
      'class'              => '',
      'taxonomy'           => 'category',
      'value_field'        => 'term_id',
    );
    $sldr_select = wp_dropdown_categories( $args );

    $name = $this->get_field_name( "slider_cat_count" );
    $sldr_count = '<input type="number" name="'. $name .'" min="1" max="5" value="'. $slider_cat_count .'">';

    // Display the admin form
    include( plugin_dir_path(__FILE__) . 'views/widget_category_slider_xxi-admin.php' );
  } // end form


  /*--------------------------------------------------*/
  /* Public Functions
  /*--------------------------------------------------*/

  /**
   * Loads the Widget's text domain for localization and translation.
   */
  public function widget_textdomain() {
    load_plugin_textdomain( $this->get_widget_slug(), false, plugin_dir_path( __FILE__ ) . 'lang/' );
  } // end widget_textdomain

  /**
   * Fired when the plugin is activated.
   *
   * @param  boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
   */
  public function activate( $network_wide ) {
    // TODO define activation functionality here
  } // end activate

  /**
   * Fired when the plugin is deactivated.
   *
   * @param boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog
   */
  public function deactivate( $network_wide ) {
    // TODO define deactivation functionality here
  } // end deactivate

  /**
   * Registers and enqueues admin-specific styles.
   */
  public function register_admin_styles() {

    //wp_enqueue_style( $this->get_widget_slug().'-admin-styles', plugins_url( 'css/admin.css', __FILE__ ) );

  } // end register_admin_styles

  /**
   * Registers and enqueues admin-specific JavaScript.
   */
  public function register_admin_scripts() {

    //wp_enqueue_script( $this->get_widget_slug().'-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array('jquery') );

  } // end register_admin_scripts

  /**
   * Registers and enqueues widget-specific styles.
   */
  public function register_widget_styles() {

    //wp_enqueue_style( $this->get_widget_slug().'-widget-styles', plugins_url( 'css/widget.css', __FILE__ ) );

  } // end register_widget_styles

  /**
   * Registers and enqueues widget-specific scripts.
   */
  public function register_widget_scripts() {

    wp_enqueue_script( $this->get_widget_slug().'-script', plugins_url( '../js/widget.js', __FILE__ ), array('jquery') );

  } // end register_widget_scripts

} // end class
